<?php

namespace Drupal\commerce_spgateway\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_spgateway\Spgateway;
use Drupal\commerce_payment\Exception\PaymentGatewayException;

/**
 * Provides the Spgateway Credit Card offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "spgateway_credit_card",
 *   label = @Translation("Spgateway Credit Card"),
 *   display_label = @Translation("Credit Card"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_spgateway\PluginForm\CreditCardRedirectCheckoutForm",
 *   },
 * )
 */
class CreditCardPayment extends OffsitePaymentGatewayBase implements CreditCardPaymentInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OffsitePayPlug object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('entity_type.manager'),
          $container->get('plugin.manager.commerce_payment_type'),
          $container->get('plugin.manager.commerce_payment_method_type'),
          $container->get('datetime.time')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchantId' => '',
      'hashKey' => '',
      'hashIv' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchantId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MerchantID'),
      '#description' => $this->t('Merchant ID provided by the Newebpay.'),
      '#default_value' => $this->configuration['merchantId'],
      '#required' => TRUE,
    ];

    $form['hashKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Key'),
      '#description' =>
      $this->t('Hash Key provided by the Newebpay.'),
      '#default_value' => $this->configuration['hashKey'],
      '#required' => TRUE,
    ];

    $form['hashIv'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash IV'),
      '#description' => $this->t('Hash IV provided by the Newebpay.'),
      '#default_value' => $this->configuration['hashIv'],
      '#required' => TRUE,
    ];

    $form['storeComment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Store Comment'),
      '#description' => $this->t('The description want to show in the Newebpay payment interface.'),
      '#default_value' => $this->configuration['storeComment'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchantId'] = $values['merchantId'];
      $this->configuration['hashKey'] = $values['hashKey'];
      $this->configuration['hashIv'] = $values['hashIv'];
      $this->configuration['storeComment'] = $values['storeComment'];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function onNotify(Request $request) {

  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $rtnPostData = $request->request->all();
    $spgateway = new Spgateway();
    $tradeInfo = $rtnPostData['TradeInfo'];
    $tradeSha = $rtnPostData['TradeSha'];
    $encryptData = $spgateway->hash256dataEncrypt($tradeInfo, $this->configuration);
    if ($tradeSha !== $encryptData) {
      throw new PaymentGatewayException('tradeSha not the same.');
    }

    // Decrypt the post data.
    $decryptData = $spgateway->generateDecryptedData($tradeInfo, $this->configuration);
    $result = $decryptData['Result'];
    $merchantTradeNo = $result['MerchantOrderNo'];
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getBalance(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $merchantTradeNo,
      'remote_state' => $decryptData['Status'],
    ]);
    $payment->save();
    $payment_state = $payment->getState();
    $payment_state_transition = $payment_state->getTransitions();
    // Not succeed.
    if ($decryptData['Status'] !== 'SUCCESS') {
      // Apply the void transition.
      $payment_state->applyTransition($payment_state_transition['void']);
      $payment->save();
      throw new PaymentGatewayException('Error Code: ' . $decryptData["Status"] . '. Return Message: ' . $decryptData["Message"]);
    }
    $payment_state->applyTransition($payment_state_transition['capture']);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
  }

}
