<?php

namespace Drupal\commerce_spgateway\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_spgateway\Spgateway;

/**
 * This is the redirect form lead the user to payment service.
 */
class CreditCardRedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface $gatewayPlugin */
    $gatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $mode = $gatewayPlugin->getMode();
    $configuration = $gatewayPlugin->getConfiguration();
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $payment->getOrder();

    $spgateway = new Spgateway();
    $rdtUrl = $spgateway->getApiUrl($mode);
    $returnUrl = $form['#return_url'];
    $cancelUrl = $form['#cancel_url'];
    $data = $spgateway->generatePostData($gatewayPlugin, $configuration, $order, $returnUrl, $cancelUrl);
    return $this->buildRedirectForm(
      $form,
      $form_state,
      $rdtUrl,
      $data,
      self::REDIRECT_POST
    );
  }

}
