<?php

namespace Drupal\commerce_spgateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\Component\Serialization\Json;

/**
 * This class is used for generate the spgateway corresponding function.
 */
class Spgateway {

  const DEMO_API_URL = 'https://ccore.newebpay.com/MPG/mpg_gateway';
  const LIVE_API_URL = 'https://core.newebpay.com/MPG/mpg_gateway';
  const VERSION = '1.5';
  const RESPOND_TYPE = 'JSON';

  /**
   * Get the api url of the spgateway payment.
   *
   * @param string $mode
   *   Get current payment gateway mode.
   *
   * @return string
   *   Return the api url link.
   */
  public function getApiUrl($mode = NULL) {
    switch ($mode) {
      case 'live':
        $apiUrl = $this::LIVE_API_URL;
        break;

      default:
      case 'demo':
        $apiUrl = $this::DEMO_API_URL;
        break;
    }
    return $apiUrl;
  }

  /**
   * Generate the data what to pass to spgateway.
   *
   * @param \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $paymentGateway
   *   The payment method want to generate.
   * @param array $configuration
   *   The configuration of current payment gateway.
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order entity.
   * @param string $returnUrl
   *   The payment return url.
   * @param string $cancelUrl
   *   The payment cancel url.
   *
   * @return array
   *   The data want to post.
   */
  public function generatePostData(OffsitePaymentGatewayInterface $paymentGateway, array $configuration, Order $order, $returnUrl, $cancelUrl) {
    $paymentPluginId = $paymentGateway->getPluginId();
    $merchantOrderNo = date('YmdHis') . $order->id();
    $amount = round($order->getTotalPrice()->getNumber());
    // Add order items to the sending object.
    $items = $order->getItems();
    $sendItems = [];
    foreach ($items as $item) {
      /** @var \Drupal\commerce_order\Entity\OrderItem $item */
      $itemName = $item->getTitle();
      $itemQuantity = (int) $item->getQuantity();
      $itemTotalPrice = (int) $item->getTotalPrice()->getNumber();
      $sendItems[] = "${itemName} NT.${itemTotalPrice} x ${itemQuantity}";
    }
    $itemDesc = implode(",", $sendItems);
    $data = [
      'MerchantID' => $configuration['merchantId'],
      'TradeInfo' => '',
      'TradeSha' => '',
      'Version' => $this::VERSION,
      'RespondType' => $this::RESPOND_TYPE,
      'TimeStamp' => \Drupal::time()->getCurrentTime(),
      'LangType' => 'zh-tw',
      'MerchantOrderNo' => $merchantOrderNo,
      'Amt' => $amount,
      'ItemDesc' => $itemDesc,
      'TradeLimit' => '',
      'ExpireDate' => '',
      // 我們採用背景回傳的方式來完成付款方式，所以不用ReturnURL。.
      'ReturnURL' => $returnUrl,
      'NotifyURL' => '',
      // 非即時交易支付方式.
      'CustomerURL' => '',
      'ClientBackURL' => '',
      'Email' => $order->getEmail(),
      // 不提供修改Email.
      'EmailModify' => 0,
      // 不需用登入藍新金流會員.
      'LoginType' => 0,
      'OrderComment' => $configuration['storeComment'],
      'CREDIT' => ($paymentPluginId == "spgateway_credit_card") ? 1 : "",
      'ANDROIDPAY' => '',
      'SAMSUNGPAY' => '',
      'InstFlag' => '',
      'CreditRed' => '',
      'UNIONPAY' => '',
      'WEBATM' => '',
      'VACC' => '',
      'CVS' => '',
      'BARCODE' => '',
      'P2G' => '',
      'CVSCOM' => '',
    ];
    // Encrypt data.
    $encryptData = $this->generateEncryptedData($data, $configuration['hashKey'], $configuration['hashIv']);
    $data['TradeInfo'] = $encryptData['TradeInfo'];
    $data['TradeSha'] = $encryptData['TradeSha'];
    return $data;

  }

  /**
   * Generate encrypted data for payment transaction.
   *
   * @param array $data
   *   The data want to encrypted.
   * @param string $hashKey
   *   The hashKey in configuration.
   * @param string $hashIv
   *   The hashIv in configuration.
   *
   * @return array
   *   The tradeinfo and tradesha data needed for passing to spgateway.
   */
  private function generateEncryptedData(array $data, $hashKey = '', $hashIv = '') {

    $tradeInfo = $this->createMpgAesEncrypt($data, $hashKey, $hashIv);
    $hashData = 'HashKey=' . $hashKey . '&' . $tradeInfo . '&HashIV=' . $hashIv;
    $tradeSha = strtoupper(hash("sha256", $hashData));

    $output['TradeInfo'] = $tradeInfo;
    $output['TradeSha'] = $tradeSha;

    return $output;
  }

  /**
   * Decrypt the return data.
   *
   * @param string $TradeInfo
   *   智付寶回傳的加密訊息.
   * @param array $configuration
   *   The configuration of current payment gateway.
   *
   * @return array
   *   回傳解密後的陣列.
   */
  public function generateDecryptedData($tradeInfo, array $configuration) {
    $hashKey = $configuration['hashKey'];
    $hashIv = $configuration['hashIv'];
    $output = $this->createAesDecrypt($tradeInfo, $hashKey, $hashIv);

    return Json::decode($output);
  }

  /**
   * Spgateway encrypted function.
   *
   * @param array $parameter
   *   The data want to use openssl encrypt.
   * @param string $key
   *   The hashKey.
   * @param string $iv
   *   The hashIv.
   */
  private function createMpgAesEncrypt(array $parameter = [], $key = "", $iv = "") {
    $return_str = '';
    if (!empty($parameter)) {
      $return_str = http_build_query($parameter);
    }
    return trim(bin2hex(openssl_encrypt($this->addPadding($return_str), 'aes-256-cbc', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv)));
  }

  /**
   * Spgateway encrypted function.
   *
   * @param string $string
   *   Some variable.
   * @param string $blocksize
   *   Some variable.
   */
  private function addPadding($string, $blocksize = 32) {
    $len = strlen($string);
    $pad = $blocksize - ($len % $blocksize);
    $string .= str_repeat(chr($pad), $pad);

    return $string;
  }

  /**
   * 智付通解密函數.
   */
  private function createAesDecrypt($parameter = "", $key = "", $iv = "") {
    return $this->strippadding(openssl_decrypt(hex2bin($parameter), 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv));
  }

  /**
   * 智付通解密函數.
   */
  private function strippadding($string) {
    $slast = ord(substr($string, -1));
    $slastc = chr($slast);
    $pcheck = substr($string, -$slast);
    if (preg_match("/$slastc{" . $slast . "}/", $string)) {
      $string = substr($string, 0, strlen($string) - $slast);
      return $string;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param string $textAES
   *   AES加密字串.
   * @param array $configuration
   *   The configuration of current payment gateway.
   *
   * @return string
   *   SHA256加密字串.
   */
  public function hash256dataEncrypt($textAES, array $configuration) {

    $hashKey = $configuration['hashKey'];
    $hashIv = $configuration['hashIv'];
    $output = 'HashKey=' . $hashKey . '&' . $textAES . '&HashIV=' . $hashIv;
    return strtoupper(hash("sha256", $output));
    ;
  }

}
